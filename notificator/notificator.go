package notificator

import (
	"fmt"
	"net/http"

	nmodel "ketitik/shared-service/hermes-notificator/model"
)

const (
	// TelegramBot holds the Factory-Method's index for telegram bot notificator
	TelegramBot = "telegram-bot"
	// SMTP holds the Factory-Method's index for SMTP notificator
	SMTP = "email-smtp"
)

// Notificator is the signature's method for each scrapper.
type Notificator interface {
	HandlePOST(w http.ResponseWriter, r *http.Request)
	GetID() string
}

// NewNotificator returns a set of node scrapper.
func NewNotificator(notification nmodel.Notification) (Notificator, error) {
	switch notification.Type {
	case TelegramBot:
		notificator, err := NewTelebotNotificator(notification)
		if err != nil {
			return nil, err
		}
		return notificator, nil
	case SMTP:
		notificator, err := NewSMTPNotificator(notification)
		if err != nil {
			return nil, err
		}
		return notificator, nil
	}

	return nil, fmt.Errorf("Invalid notification type")
}
