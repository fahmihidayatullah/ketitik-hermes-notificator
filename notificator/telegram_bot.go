package notificator

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gomodules/notify/telegram"
	"github.com/sirupsen/logrus"

	nrespwriter "ketitik/shared-service/hermes-notificator/lib/responsewriter"
	nmodel "ketitik/shared-service/hermes-notificator/model"
)

// TelegramBotNotificator is the structure of Telegram Bot Notificator.
type TelegramBotNotificator struct {
	id         string
	name       string
	token      string
	recipients []string
}

// NewTelebotNotificator creates Notificator to Telegram Bot instance.
func NewTelebotNotificator(notification nmodel.Notification) (Notificator, error) {
	id := notification.ID.Hex()
	name := notification.Name
	if name == "" {
		return nil, fmt.Errorf("Unable to find notificator's name configuration")
	}
	config := notification.Config
	token, ok := config["bot_token"].(string)
	if !ok {
		return nil, fmt.Errorf("Unable to find bot token configuration")
	}

	var recipients []string
	recipByte, _ := json.Marshal(config["recipients"])
	json.Unmarshal(recipByte, &recipients)
	if len(recipients) <= 0 {
		return nil, fmt.Errorf("There is no recipient in the configuration")
	}
	notificator := &TelegramBotNotificator{
		id:         id,
		name:       name,
		token:      token,
		recipients: recipients,
	}
	return notificator, nil
}

// GetID returns the instance's id.
func (n *TelegramBotNotificator) GetID() string {
	return n.id
}

// HandlePOST handles the webhook endpoint for this notificator instance.
func (n *TelegramBotNotificator) HandlePOST(w http.ResponseWriter, r *http.Request) {
	source := r.Host
	rf := &nrespwriter.ResponseFormat{}
	decoder := json.NewDecoder(r.Body)
	paramMsg := nmodel.NotificationMessage{}
	err := decoder.Decode(&paramMsg)
	if err != nil {
		rf.ResponseNOK(400, nil, w)
		logrus.Errorf("(%v) Webhook-Source: %v | Unparsed Message | Status: %v", n.name, source, err)
		return
	}
	subject := paramMsg.Subject
	bodyLen := len(paramMsg.Body)

	botOption := telegram.Options{
		Token:   n.token,
		Channel: n.recipients,
	}
	botClient := telegram.New(botOption)
	message := fmt.Sprintf("%v\n\n%v", paramMsg.Subject, paramMsg.Body)
	err = botClient.WithBody(message).Send()
	if err != nil {
		errorStatement := fmt.Sprintf("Unable to send message: %v", err)
		rf.ResponseNOK(501, errorStatement, w)
		logrus.Errorf("(%v) Webhook-Source: %v | Subject: %v | BodyLen: %v | Status: %v", n.name, source, subject, bodyLen, err)
		return
	}

	rf.ResponseOK(200, nil, w)
	return
}
