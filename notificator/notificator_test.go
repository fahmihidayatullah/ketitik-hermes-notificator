package notificator_test

import (
	"testing"

	"gopkg.in/mgo.v2/bson"

	nmodel "ketitik/shared-service/hermes-notificator/model"
	nnotif "ketitik/shared-service/hermes-notificator/notificator"
)

func TestNewNotificatorTelegramBot(t *testing.T) {
	t.Run("TelegramBot-OK", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "telebot-1",
			Type: nnotif.TelegramBot,
			Config: map[string]interface{}{
				"bot_token": "aaabbbccc",
				"recipients": []string{
					"-123456",
					"-098765",
				},
			},
		}
		_, err := nnotif.NewNotificator(notification)
		if err != nil {
			t.Fatalf("It should be OK: %v", err)
		}
	})
	t.Run("TelegramBot-NOK", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "telebot-1",
			Type: nnotif.TelegramBot,
			Config: map[string]interface{}{
				"wrong":      "wrong",
				"recipients": []string{},
			},
		}
		_, err := nnotif.NewNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK")
		}
	})
}

func TestNewNotificatorSNMP(t *testing.T) {
	t.Run("SNMP-OK", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "email-1",
			Type: nnotif.SMTP,
			Config: map[string]interface{}{
				"host":     "smtp.mail.bob.com",
				"port":     float64(465),
				"username": "hoho",
				"password": "hihi",
				"from":     "hoho@mail.bob.com",
				"sent_to": []string{
					"something@anything.nothing",
					"calculus@statistics.math",
				},
			},
		}

		_, err := nnotif.NewNotificator(notification)
		if err != nil {
			t.Fatalf("It should be OK: %v", err)
		}
	})
	t.Run("SNMP-NOK", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "email-1",
			Type: nnotif.SMTP,
			Config: map[string]interface{}{
				"wrong":      "wring",
				"recipients": []string{},
			},
		}

		_, err := nnotif.NewNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK")
		}
	})
}

func TestNewNotificatorNoType(t *testing.T) {
	notification := nmodel.Notification{}
	_, err := nnotif.NewNotificator(notification)
	if err == nil {
		t.Fatalf("It should be NOK.")
	}
}
