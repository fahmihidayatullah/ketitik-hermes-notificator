package notificator_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"testing"

	nmodel "ketitik/shared-service/hermes-notificator/model"
	nnotif "ketitik/shared-service/hermes-notificator/notificator"
	ntesting "ketitik/shared-service/hermes-notificator/testing"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func TestNewTelebotNotificator(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "telebot-1",
			Config: map[string]interface{}{
				"bot_token": "aaabbbccc",
				"recipients": []string{
					"-12345", "-09876",
				},
			},
		}
		telebotNotif, err := nnotif.NewTelebotNotificator(notification)
		if err != nil {
			t.Fatalf("It should be OK: %v", err)
		}
		if telebotNotif == nil {
			t.Fatalf("It should be not nil")
		}
	})
	t.Run("NOK-NoName", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "",
			Config: map[string]interface{}{
				"bot_token": "aaabbbccc",
				"recipients": []string{
					"-12345", "-09876",
				},
			},
		}
		_, err := nnotif.NewTelebotNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
	t.Run("NOK-WrongBotTokenKey", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "exists",
			Config: map[string]interface{}{
				"bot-token": "aaabbbccc",
			},
		}
		_, err := nnotif.NewTelebotNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
	t.Run("NOK-NoRecipientIDFound", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "exists",
			Config: map[string]interface{}{
				"bot_token": "aaabbbccc",
			},
		}
		_, err := nnotif.NewTelebotNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
}

func TestGetIDTelebot(t *testing.T) {
	notification := nmodel.Notification{
		ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
		Name: "telebot-1",
		Config: map[string]interface{}{
			"bot_token": "aaabbbccc",
			"recipients": []string{
				"-12345", "-09876",
			},
		},
	}
	telebotNotif, err := nnotif.NewTelebotNotificator(notification)
	if err != nil {
		t.Fatalf("It should be OK: %v", err)
	}
	if telebotNotif.GetID() != notification.ID.Hex() {
		t.Errorf("The telegram bot instance's id should be %v instead of %v", notification.ID.Hex(), telebotNotif.GetID())
	}
}

func TestHandlePOSTTelegramBot(t *testing.T) {
	notification := nmodel.Notification{
		ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
		Name: "telebot-1",
		Config: map[string]interface{}{
			"bot_token": "aaabbbccc",
			"recipients": []string{
				"-12345", "-09876",
			},
		},
	}
	telebotNotif, err := nnotif.NewTelebotNotificator(notification)
	if err != nil {
		t.Fatalf("It should be OK: %v", err)
	}
	notificators := []nnotif.Notificator{telebotNotif}

	// Prepare the HTTP Server Mock
	r := nnotif.NewRouter(notificators)
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Try Hit API Endpoint
	targetPath := fmt.Sprintf("%v/notificator/%v", serverURL, telebotNotif.GetID())
	req, _ := http.NewRequest("POST", targetPath, nil)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to get worker status: %v", err)
	}
	resp.Body.Close()

	// 1st Scenario, OK
	t.Run("POST-OK", func(t *testing.T) {
		// Hit API Endpoint
		var jsonRequest = []byte(`{"subject":"Test Sending Telgram Notification","body":"Regards/n**Ketitik**"}`)
		req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to post webhook: %v", err)
		}
		if resp.StatusCode != 200 {
			t.Fatalf("Response code %v", resp.StatusCode)
		}
		resp.Body.Close()
	})

	// 2nd Scenario, NOK
	t.Run("POST-NOK", func(t *testing.T) {
		// Hit API Endpoint
		var jsonRequest = []byte(``)
		req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to post webhook: %v", err)
		}
		if resp.StatusCode != 400 {
			t.Fatalf("Response code %v", resp.StatusCode)
		}
		resp.Body.Close()
	})
}

// 3rd Scenario, NOK
func TestHandlePOSTNOKTelegramBot(t *testing.T) {
	telebotNotif := &nnotif.TelegramBotNotificator{}

	// Prepare the HTTP Server Mock
	r := mux.NewRouter()
	r.HandleFunc("/notificator/test-bot", telebotNotif.HandlePOST).Methods("POST")
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Hit API Endpoint
	targetPath := fmt.Sprintf("%v/notificator/%v", serverURL, "test-bot")
	var jsonRequest = []byte(`{"subject":"Test Sending Telgram Notification","body":"Regards/n**Ketitik**"}`)
	req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to post webhook: %v", err)
	}
	if resp.StatusCode != 501 {
		t.Fatalf("Response code %v", resp.StatusCode)
	}
	resp.Body.Close()
}
