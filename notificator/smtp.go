package notificator

import (
	"encoding/json"
	"fmt"
	"net/http"

	nrespwriter "ketitik/shared-service/hermes-notificator/lib/responsewriter"
	nmodel "ketitik/shared-service/hermes-notificator/model"

	xsmtp "github.com/gomodules/notify/smtp"
	"github.com/sirupsen/logrus"
	xnotify "gomodules.xyz/notify"
)

// SMTPNotificator is the structure of SMTP Notificator.
type SMTPNotificator struct {
	id     string
	name   string
	client xnotify.ByEmail
}

// NewSMTPNotificator creates Notificator to SMTP instance.
func NewSMTPNotificator(notif nmodel.Notification) (Notificator, error) {
	id := notif.ID.Hex()
	name := notif.Name
	if name == "" {
		return nil, fmt.Errorf("Unable to find notificator's name configuration")
	}
	config := notif.Config
	host, ok := config["host"].(string)
	if !ok || host == "" {
		return nil, fmt.Errorf("Unable to find host configuration")
	}
	port, ok := config["port"].(float64)
	if !ok {
		return nil, fmt.Errorf("Unable to find port configuration")
	}
	from, ok := config["from"].(string)
	if !ok || from == "" {
		return nil, fmt.Errorf("Unable to find from configuration")
	}
	username, _ := config["username"].(string)
	password, _ := config["password"].(string)

	var sentTo []string
	sentToByte, _ := json.Marshal(config["sent_to"])
	json.Unmarshal(sentToByte, &sentTo)
	if len(sentTo) <= 0 {
		return nil, fmt.Errorf("There is no sent_to in the configuration")
	}
	notificatorOpt := xsmtp.Options{
		Host:               host,
		Port:               int(port),
		From:               from,
		Username:           username,
		Password:           password,
		InsecureSkipVerify: true,
		To:                 sentTo,
	}
	notificator := &SMTPNotificator{
		id:     id,
		name:   name,
		client: xsmtp.New(notificatorOpt),
	}
	return notificator, nil
}

// GetID returns the instance's id.
func (n *SMTPNotificator) GetID() string {
	return n.id
}

// HandlePOST handles the webhook endpoint for this notificator instance.
func (n *SMTPNotificator) HandlePOST(w http.ResponseWriter, r *http.Request) {
	source := r.Host
	rf := &nrespwriter.ResponseFormat{}
	decoder := json.NewDecoder(r.Body)
	paramMsg := nmodel.NotificationMessage{}
	err := decoder.Decode(&paramMsg)
	if err != nil {
		rf.ResponseNOK(400, nil, w)
		logrus.Errorf("(%v) Webhook-Source: %v | Unparsed Message | Status: %v", n.name, source, err)
		return
	}
	subject := paramMsg.Subject
	bodyLen := len(paramMsg.Body)

	smtpClient := n.client
	message := fmt.Sprintf("%v", paramMsg.Body)
	err = smtpClient.WithSubject(subject).WithBody(message).Send()
	if err != nil {
		errorStatement := fmt.Sprintf("Unable to send message: %v", err)
		rf.ResponseNOK(501, errorStatement, w)
		logrus.Errorf("(%v) Webhook-Source: %v | Subject: %v | BodyLen: %v | Status: %v", n.name, source, subject, bodyLen, err)
		return
	}

	rf.ResponseOK(200, nil, w)
	return
}
