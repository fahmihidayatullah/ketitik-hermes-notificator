package notificator

import (
	"context"
	"fmt"
	"net/http"

	nconfig "ketitik/shared-service/hermes-notificator/config"
	nmodel "ketitik/shared-service/hermes-notificator/model"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

const (
	// ConfigFileLocation is the file configuration of ths service.
	ConfigFileLocation = "notification.yaml"
)

// MongoDB presents the interface for mongodb instance.
type MongoDB interface {
	NotificationGetAll() ([]nmodel.Notification, error)
}

//NewNotifManager initiate new NotifManager struct
func NewNotifManager(mongodb MongoDB, configFileLocation string) *NotifManager {
	return &NotifManager{
		mongodb:            mongodb,
		configFileLocation: configFileLocation,
		restartChan:        make(chan bool, 1),
	}
}

//NotifManager implements manager interface
type NotifManager struct {
	mongodb            MongoDB
	configFileLocation string
	restartChan        chan bool
}

// Start runs notificator http server
func (nm *NotifManager) Start() error {
	for {
		configLoader := nconfig.NewYAMLConfigLoader(nm.configFileLocation)
		config, err := configLoader.GetConfig()
		if err != nil {
			logrus.Errorf("Unable to load configuration: %v", err)
			return err
		}

		notifications, err := nm.mongodb.NotificationGetAll()
		if err != nil {
			logrus.Errorf("Unable to get all notification from db: %v", err)
			return err
		}

		notificators := []Notificator{}
		for _, notification := range notifications {
			notificator, err := NewNotificator(notification)
			if err != nil {
				logrus.Errorf("Unable to create notificator '%v': %v", notification.Name, err)
				continue
			}
			notificators = append(notificators, notificator)
		}

		r := NewRouter(notificators)
		srv := &http.Server{
			Addr:    config.ServiceData.NotificatorAddress,
			Handler: r,
		}

		go func() {
			<-nm.restartChan

			logrus.Printf("Shutdown notificator http server")
			if err := srv.Shutdown(context.Background()); err != nil {
				logrus.Errorf("Error shutdown notificator http server: %v", err)
			}
		}()

		logrus.Printf("Starting notificator http server at %v", config.ServiceData.NotificatorAddress)
		err = srv.ListenAndServe()
		if err != nil {
			logrus.Errorf("Error notificator http server: %v", err)
		}

		logrus.Printf("Restart notificator http server")
	}
}

// Restart restart notificator http server
func (nm *NotifManager) Restart() {
	nm.restartChan <- true
}

//NewRouter creates dynamic notification endpoint based on data in database
func NewRouter(notificators []Notificator) *mux.Router {
	r := mux.NewRouter()
	for _, notificator := range notificators {
		path := fmt.Sprintf("/notificator/%v", notificator.GetID())
		r.HandleFunc(path, notificator.HandlePOST).Methods("POST")
		logrus.Printf("Route added: %v", path)
	}
	return r
}
