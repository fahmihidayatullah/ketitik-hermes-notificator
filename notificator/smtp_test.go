package notificator_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"testing"

	nmodel "ketitik/shared-service/hermes-notificator/model"
	nnotif "ketitik/shared-service/hermes-notificator/notificator"
	ntesting "ketitik/shared-service/hermes-notificator/testing"

	"gopkg.in/mgo.v2/bson"
)

func TestNewSMTPNotificator(t *testing.T) {
	t.Run("OK", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
			Name: "email-1",
			Config: map[string]interface{}{
				"host":     "smtp.mail.bob.com",
				"port":     float64(465),
				"username": "hoho",
				"password": "hihi",
				"from":     "hoho@mail.bob.com",
				"sent_to": []string{
					"something@anything.nothing", "calculus@statistics.math",
				},
			},
		}
		smtpNotif, err := nnotif.NewSMTPNotificator(notification)
		if err != nil {
			t.Fatalf("It should be OK: %v", err)
		}
		if smtpNotif == nil {
			t.Fatalf("It should be not nil")
		}
	})
	t.Run("NOK-NoName", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
			Name: "",
			Config: map[string]interface{}{
				"host":     "smtp.mail.bob.com",
				"port":     float64(465),
				"username": "hoho",
				"password": "hihi",
				"from":     "hoho@mail.bob.com",
				"sent_to": []string{
					"something@anything.nothing", "calculus@statistics.math",
				},
			},
		}
		_, err := nnotif.NewSMTPNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
	t.Run("NOK-EmptyHost", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
			Name: "email-1",
			Config: map[string]interface{}{
				"host":     "",
				"port":     float64(465),
				"username": "hoho",
				"password": "hihi",
				"from":     "hoho@mail.bob.com",
				"sent_to": []string{
					"something@anything.nothing", "calculus@statistics.math",
				},
			},
		}
		_, err := nnotif.NewSMTPNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
	t.Run("NOK-EmptyPort", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
			Name: "email-1",
			Config: map[string]interface{}{
				"host":     "smtp.mail.bob.com",
				"port":     "",
				"username": "hoho",
				"password": "hihi",
				"from":     "hoho@mail.bob.com",
				"sent_to": []string{
					"something@anything.nothing", "calculus@statistics.math",
				},
			},
		}
		_, err := nnotif.NewSMTPNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
	t.Run("NOK-PortNotInteger", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
			Name: "email-1",
			Config: map[string]interface{}{
				"host":     "smtp.mail.bob.com",
				"port":     "abc",
				"username": "hoho",
				"password": "hihi",
				"from":     "hoho@mail.bob.com",
				"sent_to": []string{
					"something@anything.nothing", "calculus@statistics.math",
				},
			},
		}
		_, err := nnotif.NewSMTPNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
	t.Run("NOK-EmptyFrom", func(t *testing.T) {
		notification := nmodel.Notification{
			ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
			Name: "email-1",
			Config: map[string]interface{}{
				"host":     "smtp.mail.bob.com",
				"port":     float64(465),
				"username": "hoho",
				"password": "hihi",
				"from":     "",
				"sent_to": []string{
					"something@anything.nothing", "calculus@statistics.math",
				},
			},
		}
		_, err := nnotif.NewSMTPNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
	t.Run("NOK-NoRecipientIDFound", func(t *testing.T) {
		notification := nmodel.Notification{
			Name: "email-1",
			Config: map[string]interface{}{
				"host":     "smtp.mail.bob.com",
				"port":     float64(465),
				"username": "hoho",
				"password": "hihi",
				"from":     "hoho@mail.bob.com",
			},
		}
		_, err := nnotif.NewSMTPNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
	t.Run("NOK-NoPortFound", func(t *testing.T) {
		notification := nmodel.Notification{
			Name: "email-1",
			Config: map[string]interface{}{
				"host":     "smtp.mail.bob.com",
				"username": "hoho",
				"password": "hihi",
				"from":     "hoho@mail.bob.com",
			},
		}
		_, err := nnotif.NewSMTPNotificator(notification)
		if err == nil {
			t.Fatalf("It should be NOK.")
		}
	})
}

func TestGetNameSMTP(t *testing.T) {
	notification := nmodel.Notification{
		ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
		Name: "email-1",
		Config: map[string]interface{}{
			"host":     "smtp.mail.bob.com",
			"port":     float64(465),
			"username": "hoho",
			"password": "hihi",
			"from":     "hoho@mail.bob.com",
			"sent_to": []string{
				"something@anything.nothing", "calculus@statistics.math",
			},
		},
	}
	smtpNotif, err := nnotif.NewSMTPNotificator(notification)
	if err != nil {
		t.Fatalf("It should be OK: %v", err)
	}
	if smtpNotif.GetID() != notification.ID.Hex() {
		t.Errorf("The telegram bot instance's id should be %v instead of %v", notification.ID.Hex(), smtpNotif.GetID())
	}
}

func TestHandlePOSTSMTP(t *testing.T) {
	notification := nmodel.Notification{
		ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
		Name: "email-1",
		Config: map[string]interface{}{
			"host":     "smtp.mail.bob.com",
			"port":     float64(465),
			"username": "hoho",
			"password": "hihi",
			"from":     "hoho@mail.bob.com",
			"sent_to": []string{
				"something@anything.nothing", "calculus@statistics.math",
			},
		},
	}
	smtpNotif, err := nnotif.NewSMTPNotificator(notification)
	if err != nil {
		t.Fatalf("It should be OK: %v", err)
	}
	notificators := []nnotif.Notificator{smtpNotif}

	// Prepare the HTTP Server Mock
	r := nnotif.NewRouter(notificators)
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Try Hit API Endpoint
	targetPath := fmt.Sprintf("%v/notificator/%v", serverURL, smtpNotif.GetID())
	req, _ := http.NewRequest("POST", targetPath, nil)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to get worker status: %v", err)
	}
	resp.Body.Close()

	// 1st Scenario, supposed to be OK
	t.Run("POST-Fake-OK", func(t *testing.T) {
		// Hit API Endpoint
		var jsonRequest = []byte(`{"subject":"Test Sending Email SMTP Notification","body":"Regards/n**Ketitik**"}`)
		req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to post webhook: %v", err)
		}
		// PS: Currently, I'm unable to mock SMTP Server (Naqvi),
		// so, I think it's acceptable to just go through it and test it later in the real environment.
		// if resp.StatusCode != 200 {
		// 	t.Fatalf("Response code %v", resp.StatusCode)
		// }
		resp.Body.Close()
	})

	// 2nd Scenario, NOK
	t.Run("POST-NOK", func(t *testing.T) {
		// Hit API Endpoint
		var jsonRequest = []byte(``)
		req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to post webhook: %v", err)
		}
		if resp.StatusCode != 400 {
			t.Fatalf("Response code %v", resp.StatusCode)
		}
		resp.Body.Close()
	})
}
