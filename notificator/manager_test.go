package notificator_test

import (
	"fmt"
	"testing"
	"time"

	nmodel "ketitik/shared-service/hermes-notificator/model"
	nnotif "ketitik/shared-service/hermes-notificator/notificator"

	"gopkg.in/mgo.v2/bson"
)

const (
	ConfigFileLocation = "_fixtures/notification.yaml"
)

type MockedMongoDB struct {
	ErrMap       map[string]bool
	ErrStatement error
}

// NotificationInsert insert Notification to mongodb
func (m *MockedMongoDB) NotificationInsert(notification *nmodel.Notification) (*nmodel.Notification, error) {
	return nil, nil
}

// NotificationGetAll function to get list of Notification from mongodb
func (m *MockedMongoDB) NotificationGetAll() ([]nmodel.Notification, error) {
	if m.ErrMap["NotificationGetAll"] {
		return nil, m.ErrStatement
	}

	notifications := []nmodel.Notification{
		nmodel.Notification{
			ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
			Name: "Group-Ops",
			Type: "telegram-bot",
			Config: map[string]interface{}{
				"bot_token": "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
				"recipients": []string{
					"-246367260",
				},
			},
		},
		nmodel.Notification{
			ID:   bson.ObjectIdHex("5d6dd927d5f7ea2cfce5b353"),
			Name: "Network-Mgmt",
			Type: "email-smtp",
			Config: map[string]interface{}{
				"host":     "smtp.netmonk.id",
				"port":     25,
				"username": "test@entmonk.id",
				"password": "pass",
				"from":     "notif@netmonk.id",
				"recipients": []string{
					"prima@ketitik.com",
					"arwani@ketitik.com",
				},
			},
		},
		nmodel.Notification{
			ID:   bson.ObjectIdHex("5d6df47fd5f7ea14c9263307"),
			Name: "Network-Mgmt-2",
			Type: "email-smtp-2",
			Config: map[string]interface{}{
				"host":     "smtp.netmonk.id",
				"port":     25,
				"username": "test@entmonk.id",
				"password": "pass",
				"from":     "notif@netmonk.id",
				"recipients": []string{
					"prima@ketitik.com",
					"arwani@ketitik.com",
				},
			},
		},
	}

	return notifications, nil
}

// NotificationGetSpecific function to get specific of Notification from mongodb
func (m *MockedMongoDB) NotificationGetSpecific(id bson.ObjectId) (*nmodel.Notification, error) {
	return nil, nil
}

// NotificationUpdate update data node_inventory
func (m *MockedMongoDB) NotificationUpdate(notification *nmodel.Notification) (*nmodel.Notification, error) {
	return nil, nil
}

// NotificationDelete is function to delete Notification by id
func (m *MockedMongoDB) NotificationDelete(id bson.ObjectId) error {
	return nil
}

func TestStartRestart(t *testing.T) {
	mongodb := &MockedMongoDB{
		ErrStatement: fmt.Errorf("Error created intentionally."),
		ErrMap:       map[string]bool{},
	}

	notifManager := nnotif.NewNotifManager(mongodb, ConfigFileLocation)

	go notifManager.Start()
	time.Sleep(1 * time.Second)
	notifManager.Restart()
	time.Sleep(1 * time.Second)
}

func TestStartNOK(t *testing.T) {
	mongodb := &MockedMongoDB{
		ErrStatement: fmt.Errorf("Error created intentionally."),
		ErrMap:       map[string]bool{},
	}

	t.Run("TestStart_Config NOK", func(t *testing.T) {
		notifManager := nnotif.NewNotifManager(mongodb, "wrong-filepath")

		err := notifManager.Start()
		if err == nil {
			t.Errorf("This should be error")
		}
	})

	t.Run("TestStart_NotificationGetAll NOK", func(t *testing.T) {
		mongodb.ErrMap["NotificationGetAll"] = true
		notifManager := nnotif.NewNotifManager(mongodb, ConfigFileLocation)

		notifManager.Start()
	})
}

func TestNewRouter(t *testing.T) {
	notificator := &nnotif.TelegramBotNotificator{}
	notificators := []nnotif.Notificator{notificator}
	_ = nnotif.NewRouter(notificators)
}
