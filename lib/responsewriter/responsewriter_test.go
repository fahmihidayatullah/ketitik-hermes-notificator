package responsewriter

import (
	"net/http"
	"testing"
)

type mockedHTTPWriter struct{}

func (w *mockedHTTPWriter) Header() http.Header {
	return http.Header{}
}

func (w *mockedHTTPWriter) Write([]byte) (int, error) {
	return 0, nil
}

func (w *mockedHTTPWriter) WriteHeader(statusCode int) {
	return
}

func TestResponseOK(t *testing.T) {
	w := &mockedHTTPWriter{}
	rf := &ResponseFormat{}
	t.Run("OK", func(t *testing.T) {
		rf.ResponseOK(200, nil, w)
	})
	t.Run("NOK", func(t *testing.T) {
		data := make(chan int)
		rf.ResponseOK(501, data, w)
	})
}

func TestResponseNOK(t *testing.T) {
	w := &mockedHTTPWriter{}
	rf := &ResponseFormat{}
	t.Run("OK", func(t *testing.T) {
		rf.ResponseNOK(501, nil, w)
	})
	t.Run("NOK", func(t *testing.T) {
		data := make(chan int)
		rf.ResponseNOK(200, data, w)
	})
}

func TestGetLastPath(t *testing.T) {
	path := "first/last"
	lastPath := GetLastPath(path)
	if lastPath != "last" {
		t.Fatalf("It should be naqvi instead of %v", lastPath)
	}
}
