package mongo

import (
	"fmt"
	"time"

	mgo "gopkg.in/mgo.v2"
)

// SessionLayer explain the session of MongoDB.
type SessionLayer interface {
	Ping() error
	Refresh()
	BuildInfo() (mgo.BuildInfo, error)
	DB(dbname string) DataLayer
}

// Session of MongoDB.
type Session struct {
	session *mgo.Session
}

// Ping tests the connection of session to MongoDB.
func (s *Session) Ping() error {
	if s.session == nil {
		return fmt.Errorf("session nil")
	}
	return s.session.Ping()
}

// Refresh the session.
func (s *Session) Refresh() {
	if s.session == nil {
		return
	}
	s.session.Refresh()
}

// BuildInfo gets the MongoDB's build information.
func (s *Session) BuildInfo() (mgo.BuildInfo, error) {
	if s.session == nil {
		return mgo.BuildInfo{}, fmt.Errorf("session nil")
	}
	return s.session.BuildInfo()
}

// DB returns the DB instance.
func (s *Session) DB(dbname string) DataLayer {
	if s.session == nil {
		return nil
	}
	return &DB{s.session.DB(dbname)}
}

// DataLayer explains the DB (DataLayer)'s interface.
type DataLayer interface {
	C(name string) CollectionLayer
	CollectionNames() (names []string, err error)
}

// DB of MongoDB
type DB struct {
	db *mgo.Database
}

// C returns the collection instance.
func (db *DB) C(collection string) CollectionLayer {
	return &Collection{db.db.C(collection)}
}

// CollectionNames returns db name
func (db *DB) CollectionNames() (names []string, err error) {
	collections, err := db.db.CollectionNames()
	return collections, err
}

// CollectionLayer explains the Collection.
type CollectionLayer interface {
	Find(query interface{}) QueryLayer
	Insert(docs interface{}) error
	Update(selector interface{}, update interface{}) error
	Remove(selector interface{}) error
	Pipe(pipeline interface{}) PipeLayer
}

// Collection of MongoDB.
type Collection struct {
	c *mgo.Collection
}

// Find takes query schema object (bson) and returns query isntance.
func (c *Collection) Find(query interface{}) QueryLayer {
	if c.c == nil {
		return nil
	}
	return &Query{c.c.Find(query)}
}

// Insert inserts one or more documents in the respective collection.
func (c *Collection) Insert(docs interface{}) error {
	if c.c == nil {
		return nil
	}
	return c.c.Insert(docs)
}

// Update finds a single document matching the provided selector document and modifies it according to the update document.
func (c *Collection) Update(selector interface{}, update interface{}) error {
	if c.c == nil {
		return nil
	}
	return c.c.Update(selector, update)
}

// Remove finds a single document matching the provided selector document and removes it from the database.
func (c *Collection) Remove(selector interface{}) error {
	if c.c == nil {
		return nil
	}
	return c.c.Remove(selector)
}

// Pipe prepares a pipeline to aggregate.
func (c *Collection) Pipe(pipeline interface{}) PipeLayer {
	if c.c == nil {
		return nil
	}
	return &Pipe{c.c.Pipe(pipeline)}
}

// QueryLayer explains the query instance.
type QueryLayer interface {
	Sort(fields ...string) QueryLayer
	All(result interface{}) error
	One(result interface{}) error
	Select(selector interface{}) QueryLayer
	Count() (n int, err error)
}

// Query is the specified query of MongoDB.
type Query struct {
	q *mgo.Query
}

// All returns all of the query result.
func (q *Query) All(result interface{}) error {
	return q.q.All(result)
}

// One executes the query and unmarshals the first obtained document into the result argument.
func (q *Query) One(result interface{}) error {
	return q.q.One(result)
}

// Sort asks the database to order returned documents according to the provided field names.
func (q *Query) Sort(fields ...string) QueryLayer {
	if q.q == nil {
		return nil
	}

	return &Query{q.q.Sort(fields...)}
}

// Count returns the total number of documents in the result set.
func (q *Query) Count() (n int, err error) {
	return q.q.Count()
}

// Select enables selecting which fields should be retrieved for the results found. For example, the following query would only retrieve the name field
func (q *Query) Select(selector interface{}) QueryLayer {
	if q.q == nil {
		return nil
	}
	return &Query{q.q.Select(selector)}
}

// PipeLayer explains the pipe instance.
type PipeLayer interface {
	All(result interface{}) error
}

// Pipe is the specified pipe of MongoDB.
type Pipe struct {
	p *mgo.Pipe
}

// All returns all of the query result.
func (p *Pipe) All(result interface{}) error {
	return p.p.All(result)
}

// MongoDB 's structure.
type MongoDB struct {
	session SessionLayer
	dbname  string
}

// NewMongoDB creates MongoDB instance.
// It takes database server address, database name, database username,
// database password, and timeout tolerance in second.
func NewMongoDB(dbserver, dbname, dbuser, dbpass string, timeout int) (*MongoDB, error) {
	mongodb := new(MongoDB)
	mongodb.dbname = dbname

	// Setup MongoDB
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{dbserver},
		Timeout:  time.Duration(timeout) * time.Second,
		Database: dbname,
		Username: dbuser,
		Password: dbpass,
	}
	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		return nil, fmt.Errorf("Unable to dial Mongo DB: %v", err)
	}
	s := &Session{session}
	mongodb.SetSession(s)
	return mongodb, nil
}

// SetSession alters the session reside inside the MongoDB instance.
func (m *MongoDB) SetSession(session SessionLayer) {
	m.session = session
}

// GetSession returns the session resided inside the MongoDB instance.
func (m *MongoDB) GetSession() SessionLayer {
	if err := m.session.Ping(); err != nil {
		return m.session
	}
	m.session.Refresh()
	return m.session
}

// GetVersion returns the version of remote MongoDB Server.
func (m *MongoDB) GetVersion() (version string) {
	session := m.GetSession()
	buildInfo, err := session.BuildInfo()
	if err != nil {
		return
	}
	version = buildInfo.Version
	return
}
