package mongo

import (
	"testing"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func TestSessionPing(t *testing.T) {
	session := Session{}
	err := session.Ping()
	if err == nil {
		t.Fatal("It should be error.")
	}
}

func TestRefresh(t *testing.T) {
	session := Session{}
	session.Refresh()
}

func TestBuildInfo(t *testing.T) {
	session := Session{}
	_, err := session.BuildInfo()
	if err == nil {
		t.Fatalf("It should be error.")
	}
}

func TestSessionDB(t *testing.T) {
	session := Session{}
	db := session.DB("")
	if db != nil {
		t.Fatalf("It should be error/nil.")
	}
	sessionLayer := &mgo.Session{}
	session.session = sessionLayer
	db = session.DB("")
	if db == nil {
		t.Fatalf("There should be something, eventhough it won't work.")
	}
}

func TestDBC(t *testing.T) {
	dbLayer := &mgo.Database{}
	db := DB{}
	db.db = dbLayer
	_ = db.C("")
}

func TestCFind(t *testing.T) {
	c := Collection{}
	query := bson.M{}
	_ = c.Find(query)
}

func TestCInsert(t *testing.T) {
	c := Collection{}
	row := bson.M{}
	_ = c.Insert(row)
}

func TestCUpdate(t *testing.T) {
	c := Collection{}
	selector := bson.M{}
	update := bson.M{}
	_ = c.Update(selector, update)
}

func TestCRemove(t *testing.T) {
	c := Collection{}
	selector := bson.M{}
	_ = c.Remove(selector)
}

func TestCPipe(t *testing.T) {
	c := Collection{}
	pipeline := bson.M{}
	_ = c.Pipe(pipeline)
}

func TestCSort(t *testing.T) {
	q := Query{}
	_ = q.Sort("")
}

func TestCSelect(t *testing.T) {
	q := Query{}
	var i interface{}
	_ = q.Select(i)
}
