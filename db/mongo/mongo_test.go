package mongo_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	mgo "gopkg.in/mgo.v2"

	nmongodb "ketitik/shared-service/hermes-notificator/db/mongo"
)

const (
	IntentionallyError = "Intentionally Error."
)

func TestNewMongoDB(t *testing.T) {
	server := "localhost:27017"
	name := "name"
	user := "user"
	pass := "pass"
	timeout := 1

	mongodb, err := nmongodb.NewMongoDB(server, name, user, pass, timeout)
	// Since no connection, then return error.
	if mongodb != nil || err == nil {
		t.Fatal("No Server but return MongoDB.")
	}
}

type MockedMongoDBSession struct {
	MockedErr    error
	BuildInfoVar mgo.BuildInfo
	ErrorConf    map[string]bool
	MockedDB     *MockedDataLayer
}

func (s *MockedMongoDBSession) Ping() error {
	if s.ErrorConf["PingError"] {
		return s.MockedErr
	}
	return nil
}

func (s *MockedMongoDBSession) Refresh() {
	return
}

func (s *MockedMongoDBSession) BuildInfo() (mgo.BuildInfo, error) {
	if s.ErrorConf["BuildInfoError"] {
		return mgo.BuildInfo{}, s.MockedErr
	}
	return s.BuildInfoVar, nil
}

func (s *MockedMongoDBSession) DB(dbname string) nmongodb.DataLayer {
	return s.MockedDB
}

type MockedDataLayer struct {
	MockedCollection *MockedCollection
	MockedNames      []string
	MockedErr        error
}

func (d *MockedDataLayer) C(name string) nmongodb.CollectionLayer {
	return d.MockedCollection
}

func (s *MockedDataLayer) CollectionNames() (names []string, err error) {
	return s.MockedNames, s.MockedErr
}

type MockedCollection struct {
	MockedQuery *MockedQuery
	MockedPipe  *MockedPipe
	ErrConf     bool
}

func (c *MockedCollection) Find(query interface{}) nmongodb.QueryLayer {
	return c.MockedQuery
}

func (c *MockedCollection) Insert(docs interface{}) error {
	if c.ErrConf {
		return fmt.Errorf(IntentionallyError)
	}
	return nil
}

func (c *MockedCollection) Update(selector interface{}, update interface{}) error {
	if c.ErrConf {
		return fmt.Errorf(IntentionallyError)
	}
	return nil
}

func (c *MockedCollection) Remove(selector interface{}) error {
	if c.ErrConf {
		return fmt.Errorf(IntentionallyError)
	}
	return nil
}

func (c *MockedCollection) Pipe(pipeline interface{}) nmongodb.PipeLayer {
	return c.MockedPipe
}

type MockedQuery struct {
	MockedQuery    *MockedQuery
	ErrConf        bool
	ErrMsg         string
	ResultFileName string
	ResultCount    int
}

func (q *MockedQuery) All(result interface{}) error {
	if q.ErrConf {
		return fmt.Errorf(IntentionallyError)
	}
	jsoned := loadfixture(q.ResultFileName)
	err := json.Unmarshal(jsoned, &result)
	return err
}

func (q *MockedQuery) One(result interface{}) error {
	if q.ErrMsg == "not found" && q.ErrConf {
		return fmt.Errorf("not found")
	}
	if q.ErrConf {
		return fmt.Errorf(IntentionallyError)
	}
	jsoned := loadfixture(q.ResultFileName)
	err := json.Unmarshal(jsoned, &result)
	return err
}

func (q *MockedQuery) Count() (n int, err error) {
	if q.ErrConf {
		return 0, fmt.Errorf(IntentionallyError)
	}

	return q.ResultCount, nil
}

func (q *MockedQuery) Sort(fields ...string) nmongodb.QueryLayer {
	return q.MockedQuery
}

func (q *MockedQuery) Select(selector interface{}) nmongodb.QueryLayer {
	return q.MockedQuery
}

type MockedPipe struct {
	MockedPipe     *MockedPipe
	ErrConf        bool
	ResultFileName string
	ResultCount    int
}

func (p *MockedPipe) All(result interface{}) error {
	if p.ErrConf {
		return fmt.Errorf(IntentionallyError)
	}
	jsoned := loadfixture(p.ResultFileName)
	err := json.Unmarshal(jsoned, &result)
	return err
}

func TestSetMongoDBSession(t *testing.T) {
	// Pre-condtioned
	dummySession := &MockedMongoDBSession{
		MockedErr: fmt.Errorf("Intentionally Error"),
		ErrorConf: map[string]bool{},
	}
	dummyMongoDB := nmongodb.MongoDB{}
	dummyMongoDB.SetSession(dummySession)

	t.Run("Test Singleton", func(t *testing.T) {
		newDummySession := dummyMongoDB.GetSession()
		if newDummySession == nil {
			t.Fatal("Session should exists.")
		}
		if dummySession != newDummySession {
			t.Fatal("Both Sessions should reside the same address.")
		}
	})

	t.Run("Test Session Ping Error", func(t *testing.T) {
		// Check if Ping error
		dummySession.ErrorConf["PingError"] = true
		newDummySession := dummyMongoDB.GetSession()
		if newDummySession == nil {
			t.Fatal("Session should exists.")
		}
	})
}

func TestGetMongoDBVersion(t *testing.T) {
	// Pre-condtioned
	dummySession := &MockedMongoDBSession{
		MockedErr:    fmt.Errorf("Intentionally Error"),
		ErrorConf:    map[string]bool{},
		BuildInfoVar: mgo.BuildInfo{Version: "1.3.3.7"},
	}
	mongodb := nmongodb.MongoDB{}
	mongodb.SetSession(dummySession)

	t.Run("Check Version OK", func(t *testing.T) {
		version := mongodb.GetVersion()
		if version != "1.3.3.7" {
			t.Errorf("The Mongo's version should be 1.3.3.7 instead of %v", version)
		}
	})

	t.Run("Check Version NOK", func(t *testing.T) {
		dummySession.ErrorConf["BuildInfoError"] = true
		version := mongodb.GetVersion()
		if version != "" {
			t.Fatal("It should be error while getting mongo version.")
		}
	})
}

func loadfixture(f string) []byte {
	pwd, _ := os.Getwd()
	p := filepath.Join(pwd, ".", "_fixtures", f)
	c, _ := ioutil.ReadFile(p)
	return c
}
