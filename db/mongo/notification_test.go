package mongo_test

import (
	nmongodb "ketitik/shared-service/hermes-notificator/db/mongo"
	nmodel "ketitik/shared-service/hermes-notificator/model"
	"testing"

	"gopkg.in/mgo.v2/bson"
)

func TestNotificationInsert(t *testing.T) {
	mockedCollection := &MockedCollection{
		ErrConf: false,
	}
	mockedDB := &MockedDataLayer{
		MockedCollection: mockedCollection,
	}
	mockedSession := &MockedMongoDBSession{
		MockedDB: mockedDB,
	}

	mongodb := nmongodb.MongoDB{}
	mongodb.SetSession(mockedSession)

	t.Run("InsertNotification_OK", func(t *testing.T) {
		config := map[string]interface{}{
			"bot_token": "654321:CBA-FED1234ghIkl-zyx57W2v1u123ew11",
			"recipients": []string{
				"-726024636",
			},
		}

		notification := &nmodel.Notification{
			Name:   "Group-Ops",
			Type:   "telegram-bot",
			Config: config,
		}
		_, err := mongodb.NotificationInsert(notification)
		if err != nil {
			t.Fatalf("%v", err.Error())
		}
	})

	t.Run("InsertNotification_NOK", func(t *testing.T) {
		mockedCollection.ErrConf = true
		_, err := mongodb.NotificationInsert(nil)
		if err == nil {
			t.Fatalf("%v", err.Error())
		}
	})
}

func TestNotificationUpdate(t *testing.T) {
	mockedCollection := &MockedCollection{
		ErrConf: false,
	}
	mockedDB := &MockedDataLayer{
		MockedCollection: mockedCollection,
	}
	mockedSession := &MockedMongoDBSession{
		MockedDB: mockedDB,
	}

	mongodb := nmongodb.MongoDB{}
	mongodb.SetSession(mockedSession)

	t.Run("UpdateNotification_OK", func(t *testing.T) {
		config := map[string]interface{}{
			"bot_token": "654321:CBA-FED1234ghIkl-zyx57W2v1u123ew11",
			"recipients": []string{
				"-726024636",
			},
		}

		notification := &nmodel.Notification{
			ID:     bson.ObjectId("5977a69476371a0001a1d1ef"),
			Name:   "Group-Ops",
			Type:   "telegram-bot",
			Config: config,
		}
		_, err := mongodb.NotificationUpdate(notification)
		if err != nil {
			t.Fatalf("%v", err.Error())
		}
	})

	t.Run("UpdateNotification_NOK", func(t *testing.T) {
		mockedCollection.ErrConf = true
		config := map[string]interface{}{
			"bot_token": "654321:CBA-FED1234ghIkl-zyx57W2v1u123ew11",
			"recipients": []string{
				"-726024636",
			},
		}

		notification := &nmodel.Notification{
			ID:     bson.ObjectId("5977a69476371a0001a1d1ef"),
			Name:   "Group-Ops",
			Type:   "telegram-bot",
			Config: config,
		}
		_, err := mongodb.NotificationUpdate(notification)
		if err == nil {
			t.Fatalf("%v", err.Error())
		}
	})
}

func TestNotificationGetAll(t *testing.T) {
	mockingFile := "notification_all.json"
	mockedQuery := &MockedQuery{
		ErrConf:        false,
		ResultFileName: mockingFile,
	}
	mockedCollection := &MockedCollection{
		MockedQuery: mockedQuery,
	}
	mockedDB := &MockedDataLayer{
		MockedCollection: mockedCollection,
	}
	mockedSession := &MockedMongoDBSession{
		MockedDB: mockedDB,
	}

	mongodb := nmongodb.MongoDB{}
	mongodb.SetSession(mockedSession)

	t.Run("TestNotificationGetAll_OK", func(t *testing.T) {
		result, err := mongodb.NotificationGetAll()
		if err != nil {
			t.Fatalf("%v", err.Error())
		}
		if len(result) != 3 {
			t.Errorf("The query result length data should be 3 instead of %v", len(result))
		}
	})
	t.Run("TestNotificationGetAll_NOK", func(t *testing.T) {
		mockedQuery.ErrConf = true
		_, err := mongodb.NotificationGetAll()
		if err == nil {
			t.Fatal("It should be Error.")
		}
	})
}

func TestNotificationGetSpecific(t *testing.T) {
	mockingFile := "notification_specific.json"
	mockedQuery := &MockedQuery{
		ErrConf:        false,
		ResultFileName: mockingFile,
	}
	mockedCollection := &MockedCollection{
		MockedQuery: mockedQuery,
	}
	mockedDB := &MockedDataLayer{
		MockedCollection: mockedCollection,
	}
	mockedSession := &MockedMongoDBSession{
		MockedDB: mockedDB,
	}

	mongodb := nmongodb.MongoDB{}
	mongodb.SetSession(mockedSession)

	t.Run("TestNotificationGetSpecific_OK", func(t *testing.T) {
		result, err := mongodb.NotificationGetSpecific(bson.ObjectId("5b55832f2305a34dee3bda9c"))
		if err != nil {
			t.Fatalf("%v", err.Error())
		}
		if result.Name != "Group-Ops" {
			t.Fatalf("Notification name should be Group-Ops but have %v", result.Name)
		}
	})
	t.Run("TestNotificationGetSpecific_NOK", func(t *testing.T) {
		mockedQuery.ErrConf = true
		_, err := mongodb.NotificationGetSpecific("a")
		if err == nil {
			t.Fatal("It should be Error.")
		}
	})
}

func TestNotificationDelete(t *testing.T) {
	mockedCollection := &MockedCollection{
		ErrConf: false,
	}
	mockedDB := &MockedDataLayer{
		MockedCollection: mockedCollection,
	}
	mockedSession := &MockedMongoDBSession{
		MockedDB: mockedDB,
	}

	mongodb := nmongodb.MongoDB{}
	mongodb.SetSession(mockedSession)

	t.Run("NotificationDelete_OK", func(t *testing.T) {
		err := mongodb.NotificationDelete("5b55062e2305a34dee3bda3a")
		if err != nil {
			t.Fatalf("%v", err.Error())
		}
	})
	t.Run("TestNotificationDelete_NOK", func(t *testing.T) {
		mockedCollection.ErrConf = true
		err := mongodb.NotificationDelete("5b55062e2305a34dee3bda3a")
		if err == nil {
			t.Fatal("It should be Error.")
		}
	})
}

func TestNotificationFieldsGetAll(t *testing.T) {
	mockingFile := "notification_fields_all.json"
	mockedQuery := &MockedQuery{
		ErrConf:        false,
		ResultFileName: mockingFile,
	}
	mockedCollection := &MockedCollection{
		MockedQuery: mockedQuery,
	}
	mockedDB := &MockedDataLayer{
		MockedCollection: mockedCollection,
	}
	mockedSession := &MockedMongoDBSession{
		MockedDB: mockedDB,
	}

	mongodb := nmongodb.MongoDB{}
	mongodb.SetSession(mockedSession)

	t.Run("TestNotificationFieldsGetAll_OK", func(t *testing.T) {
		result, err := mongodb.NotificationFieldsGetAll()
		if err != nil {
			t.Fatalf("%v", err.Error())
		}
		if len(result) != 2 {
			t.Errorf("The query result length data should be 2 instead of %v", len(result))
		}
	})
	t.Run("TestNotificationFieldsGetAll_NOK", func(t *testing.T) {
		mockedQuery.ErrConf = true
		_, err := mongodb.NotificationFieldsGetAll()
		if err == nil {
			t.Fatal("It should be Error.")
		}
	})
}
