package mongo

import (
	"fmt"

	nmodel "ketitik/shared-service/hermes-notificator/model"

	"github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
)

const (
	// NotificationCollection define collection of Notification in mongodb
	NotificationCollection = "notification"
	// NotificationFieldsCollection define collection of Notification Fields in mongodb
	NotificationFieldsCollection = "notification_fields"
)

// NotificationInsert insert Notification to mongodb
func (m *MongoDB) NotificationInsert(notification *nmodel.Notification) (*nmodel.Notification, error) {
	err := m.session.DB(m.dbname).C(NotificationCollection).Insert(notification)
	if err != nil {
		logrus.Errorf("Error MongoDB Insert Notification : %v", err)
		return nil, err
	}
	return notification, nil
}

// NotificationGetAll function to get list of Notification from mongodb
func (m *MongoDB) NotificationGetAll() ([]nmodel.Notification, error) {
	result := []nmodel.Notification{}
	err := m.GetSession().DB(m.dbname).C(NotificationCollection).Find(bson.M{}).All(&result)
	if err != nil {
		return nil, fmt.Errorf("Error query Get All Notification  : %v", err)
	}

	return result, nil
}

// NotificationGetSpecific function to get specific of Notification from mongodb
func (m *MongoDB) NotificationGetSpecific(id bson.ObjectId) (*nmodel.Notification, error) {
	result := &nmodel.Notification{}
	err := m.GetSession().DB(m.dbname).C(NotificationCollection).Find(bson.M{"_id": id}).One(result)
	if err != nil {
		return nil, fmt.Errorf("Error query Get All Notification  : %v", err)
	}

	return result, nil
}

// NotificationUpdate update data node_inventory
func (m *MongoDB) NotificationUpdate(notification *nmodel.Notification) (*nmodel.Notification, error) {
	err := m.session.DB(m.dbname).C(NotificationCollection).Update(bson.M{"_id": notification.ID},
		bson.M{
			"$set": bson.M{
				"name":   notification.Name,
				"type":   notification.Type,
				"config": notification.Config,
			}},
	)
	if err != nil {
		return nil, fmt.Errorf("Error update node: %v", err)
	}

	return notification, nil
}

// NotificationDelete is function to delete Notification by id
func (m *MongoDB) NotificationDelete(id bson.ObjectId) error {
	err := m.GetSession().DB(m.dbname).C(NotificationCollection).Remove(bson.M{"_id": id})
	if err != nil {
		logrus.Errorf("Error MongoDB Delete Notification : %v", err)
		return err
	}
	return nil
}

// NotificationFieldsGetAll function to get list of Notification Fields from mongodb
func (m *MongoDB) NotificationFieldsGetAll() ([]nmodel.NotificationField, error) {
	result := []nmodel.NotificationField{}
	err := m.GetSession().DB(m.dbname).C(NotificationFieldsCollection).Find(bson.M{}).All(&result)
	if err != nil {
		return nil, fmt.Errorf("Error query Get All Notification  : %v", err)
	}

	return result, nil
}
