package model

import (
	"gopkg.in/mgo.v2/bson"
)

// Notification is notification channel scheme
type Notification struct {
	ID     bson.ObjectId          `bson:"_id" json:"id"`
	Name   string                 `bson:"name" json:"name"`
	Type   string                 `bson:"type" json:"type"`
	Config map[string]interface{} `bson:"config" json:"config"`
}

// NotificationField is dynamic configuration fields scheme
type NotificationField struct {
	ID     bson.ObjectId `bson:"_id" json:"id"`
	Type   string        `bson:"type" json:"type"`
	Label  string        `bson:"label" json:"label"`
	Fields []Field       `bson:"fields" json:"fields"`
}

// Field is detail of each dynamic configuration field scheme
type Field struct {
	Field      string `bson:"field" json:"field"`
	Label      string `bson:"label" json:"label"`
	DataType   string `bson:"data_type" json:"data_type"`
	InputType  string `bson:"input_type" json:"input_type"`
	IsRequired bool   `bson:"is_required" json:"is_required"`
}

// NotificationMessage is the scheme of webhook's body
type NotificationMessage struct {
	Subject string `json:"subject"`
	Body    string `json:"body"`
}
