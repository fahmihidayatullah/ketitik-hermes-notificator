package model

import "gopkg.in/mgo.v2/bson"

// Rule is notification rule scheme
type Rule struct {
	ID             bson.ObjectId `bson:"_id" json:"id"`
	Name           string        `json:"name"`
	Label          string        `json:"label"`
	SelectedAlert  []string      `json:"selected_alert"`
	Threshold      float64       `json:"threshold"`
	Baseline       float64       `json:"baseline"`
	CheckingPeriod int64         `json:"checking_period"`
	ReminderPeriod int64         `json:"reminder_period"`
	IsActive       bool          `json:"is_active"`
}
