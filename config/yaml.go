package config

import (
	"fmt"
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

// YAMLConfigLoader is the loader of YAML file configuration.
type YAMLConfigLoader struct {
	fileLocation string
}

// ServiceConfig stores the whole configuration for service.
type ServiceConfig struct {
	ServiceData ServiceDataConfig `yaml:"service_data"`
	SourceData  SourceDataConfig  `yaml:"source_data"`
}

// ServiceDataConfig contains the service data configuration.
type ServiceDataConfig struct {
	NotificatorAddress     string `yaml:"notificator_address"`
	NotificatorMgmtAddress string `yaml:"notificator_mgmt_address"`
}

// SourceDataConfig contains the source data configuration.
type SourceDataConfig struct {
	MongoDBServer   string `yaml:"mongodb_server"`
	MongoDBName     string `yaml:"mongodb_name"`
	MongoDBUsername string `yaml:"mongodb_username"`
	MongoDBPassword string `yaml:"mongodb_password"`
	MongoDBTimeout  int    `yaml:"mongodb_timeout"`
}

// NewYAMLConfigLoader parse the configuration from YAML file.
func NewYAMLConfigLoader(fileLocation string) *YAMLConfigLoader {
	return &YAMLConfigLoader{fileLocation}
}

func getRawConfig(fileLocation string) (*ServiceConfig, error) {
	configByte, err := ioutil.ReadFile(fileLocation)
	if err != nil {
		return nil, err
	}
	config := &ServiceConfig{}
	err = yaml.Unmarshal(configByte, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

// GetConfig parse the configuration from YAML file.
func (c *YAMLConfigLoader) GetConfig() (*ServiceConfig, error) {
	rawConfig, err := getRawConfig(c.fileLocation)
	if err != nil {
		return nil, fmt.Errorf("Unable to get raw config content: %v", err)
	}
	config := ServiceConfig(*rawConfig)
	return &config, nil
}
