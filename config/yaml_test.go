package config_test

import (
	nconfig "ketitik/shared-service/hermes-notificator/config"
	"testing"
)

func TestGetConfigOK(t *testing.T) {
	yamlFileLocation := "_fixtures/config_001.yaml"
	configLoader := nconfig.NewYAMLConfigLoader(yamlFileLocation)
	config, err := configLoader.GetConfig()
	if err != nil {
		t.Fatalf("It Should not be error: %v", err)
	}
	if config == nil {
		t.Fatal("It Should not nil")
	}
	if config.ServiceData.NotificatorAddress != "localhost:8080" {
		t.Errorf("The HTTP Address should be localhost:8080 instead of %v", config.ServiceData.NotificatorAddress)
	}
	if config.ServiceData.NotificatorMgmtAddress != "localhost:8081" {
		t.Errorf("The HTTP Address should be localhost:8080 instead of %v", config.ServiceData.NotificatorMgmtAddress)
	}
	if config.SourceData.MongoDBServer != "localhost:27017" {
		t.Fatalf("The mongodb server address should be localhost:27017 instead of %v", config.SourceData.MongoDBServer)
	}

}

func TestGetConfigNOK(t *testing.T) {
	// Error Parsing Telegram-Bot Config
	t.Run("Error-Parsing", func(t *testing.T) {
		yamlFileLocation := "_fixtures/config_002.yaml"
		configLoader := nconfig.NewYAMLConfigLoader(yamlFileLocation)
		config, err := configLoader.GetConfig()
		if err == nil {
			t.Fatal("It Should be error.")
		}
		if config != nil {
			t.Fatal("It Should be nil")
		}
	})
	// Error Read IO
	t.Run("Error-ReadIO", func(t *testing.T) {
		yamlFileLocation := "_fixtures/config_000.yaml"
		configLoader := nconfig.NewYAMLConfigLoader(yamlFileLocation)
		config, err := configLoader.GetConfig()
		if err == nil {
			t.Fatal("It Should be error.")
		}
		if config != nil {
			t.Fatal("It Should be nil")
		}
	})
}
