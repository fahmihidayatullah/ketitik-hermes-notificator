package handler

import (
	"encoding/json"
	"net/http"

	nrespwriter "ketitik/shared-service/hermes-notificator/lib/responsewriter"
	nmodel "ketitik/shared-service/hermes-notificator/model"

	"github.com/sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
)

//GetAllNotification gets all notification data
func (h *Handler) GetAllNotification(w http.ResponseWriter, r *http.Request) {
	rf := &nrespwriter.ResponseFormat{}
	data, err := h.mongodb.NotificationGetAll()
	if err != nil {
		logrus.Errorf("Error server: %v", err)
		rf.ResponseNOK(http.StatusInternalServerError, "Internal Server Error.", w)
		return
	}
	if len(data) == 0 {
		logrus.Errorf("Error not found: %v", err)
		rf.ResponseNOK(http.StatusNotFound, "Error not found.", w)
		return
	}

	rf.ResponseOK(http.StatusOK, data, w)
	return
}

//GetSpecificNotification gets single notification data
func (h *Handler) GetSpecificNotification(w http.ResponseWriter, r *http.Request) {
	rf := &nrespwriter.ResponseFormat{}
	id := nrespwriter.GetLastPath(r.URL.Path)

	checkID := bson.IsObjectIdHex(id)
	if !checkID {
		rf.ResponseNOK(http.StatusBadRequest, "not an object id", w)
		return
	}

	parsedID := bson.ObjectIdHex(id)
	data, err := h.mongodb.NotificationGetSpecific(parsedID)
	if err != nil {
		logrus.Errorf("Error server: %v", err)
		rf.ResponseNOK(http.StatusInternalServerError, "Internal Server Error.", w)
		return
	}

	rf.ResponseOK(http.StatusOK, data, w)
	return
}

type notificationParams struct {
	Name   string                 `bson:"name" json:"name"`
	Type   string                 `bson:"type" json:"type"`
	Config map[string]interface{} `bson:"config" json:"config"`
}

//InsertNotification inserts notification data
func (h *Handler) InsertNotification(w http.ResponseWriter, r *http.Request) {
	rf := &nrespwriter.ResponseFormat{}

	decoder := json.NewDecoder(r.Body)
	param := notificationParams{}

	err := decoder.Decode(&param)
	if err != nil {
		logrus.Errorf("Error Decoder Node Inventory: %v", err)
		rf.ResponseNOK(http.StatusBadRequest, "Bad Request.", w)
		return
	}

	data := &nmodel.Notification{
		ID:     bson.NewObjectId(),
		Name:   param.Name,
		Type:   param.Type,
		Config: param.Config,
	}

	notif, err := h.mongodb.NotificationInsert(data)
	if err != nil {
		logrus.Errorf("Error Insert Notification: %v", err)
		rf.ResponseNOK(http.StatusInternalServerError, "Internal Server Error.", w)
		return
	}

	h.notifManager.Restart()

	rf.ResponseOK(http.StatusOK, notif, w)
	return
}

//UpdateNotification update notification data
func (h *Handler) UpdateNotification(w http.ResponseWriter, r *http.Request) {
	rf := &nrespwriter.ResponseFormat{}
	id := nrespwriter.GetLastPath(r.URL.Path)

	decoder := json.NewDecoder(r.Body)
	param := notificationParams{}

	err := decoder.Decode(&param)
	if err != nil {
		logrus.Errorf("Error Decoder Node Inventory: %v", err)
		rf.ResponseNOK(http.StatusBadRequest, "Bad Request.", w)
		return
	}

	parsedID := bson.ObjectIdHex(id)
	data := &nmodel.Notification{
		ID:     parsedID,
		Name:   param.Name,
		Type:   param.Type,
		Config: param.Config,
	}

	notif, err := h.mongodb.NotificationUpdate(data)
	if err != nil {
		logrus.Errorf("Error Update Notification: %v", err)
		rf.ResponseNOK(http.StatusInternalServerError, "Internal Server Error.", w)
		return
	}

	h.notifManager.Restart()

	rf.ResponseOK(http.StatusOK, notif, w)
	return
}

//DeleteNotification removes notification data
func (h *Handler) DeleteNotification(w http.ResponseWriter, r *http.Request) {
	rf := &nrespwriter.ResponseFormat{}
	id := nrespwriter.GetLastPath(r.URL.Path)

	checkID := bson.IsObjectIdHex(id)
	if !checkID {
		rf.ResponseNOK(http.StatusBadRequest, "not an object id", w)
		return
	}

	parsedID := bson.ObjectIdHex(id)
	err := h.mongodb.NotificationDelete(parsedID)
	if err != nil {
		logrus.Errorf("Error Delete Notification: %v", err)
		rf.ResponseNOK(http.StatusInternalServerError, "Internal Server Error.", w)
		return
	}

	h.notifManager.Restart()

	rf.ResponseOK(http.StatusOK, nil, w)
	return
}

//GetAllNotificationFields gets all notification fields data
func (h *Handler) GetAllNotificationFields(w http.ResponseWriter, r *http.Request) {
	rf := &nrespwriter.ResponseFormat{}
	data, err := h.mongodb.NotificationFieldsGetAll()
	if err != nil {
		logrus.Errorf("Error server: %v", err)
		rf.ResponseNOK(http.StatusInternalServerError, "Internal Server Error.", w)
		return
	}
	if len(data) == 0 {
		logrus.Errorf("Error not found: %v", err)
		rf.ResponseNOK(http.StatusNotFound, "Error not found.", w)
		return
	}

	rf.ResponseOK(http.StatusOK, data, w)
	return
}
