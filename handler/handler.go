package handler

import (
	nmodel "ketitik/shared-service/hermes-notificator/model"

	"gopkg.in/mgo.v2/bson"
)

// MongoDB presents the interface for mongodb instance.
type MongoDB interface {
	NotificationGetAll() ([]nmodel.Notification, error)
	NotificationGetSpecific(id bson.ObjectId) (*nmodel.Notification, error)
	NotificationInsert(notification *nmodel.Notification) (*nmodel.Notification, error)
	NotificationUpdate(notification *nmodel.Notification) (*nmodel.Notification, error)
	NotificationDelete(id bson.ObjectId) error
	NotificationFieldsGetAll() ([]nmodel.NotificationField, error)
}

// NotifManager presents the interface for notifmanager instance.
type NotifManager interface {
	Start() error
	Restart()
}

// Handler holds the API endpoint's function handler.
type Handler struct {
	mongodb            MongoDB
	notifManager       NotifManager
	configFileLocation string
}

// NewHandler function to make connection database and manage notificaor into handler
func NewHandler(mongodb MongoDB, notifManager NotifManager, configFileLocation string) *Handler {
	return &Handler{
		mongodb:            mongodb,
		notifManager:       notifManager,
		configFileLocation: configFileLocation,
	}
}
