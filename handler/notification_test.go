package handler_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"

	nhandler "ketitik/shared-service/hermes-notificator/handler"
	nlib "ketitik/shared-service/hermes-notificator/lib"
	nrespwriter "ketitik/shared-service/hermes-notificator/lib/responsewriter"
	nmodel "ketitik/shared-service/hermes-notificator/model"
	ntesting "ketitik/shared-service/hermes-notificator/testing"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

type MockedMongoDB struct {
	ErrMap       map[string]bool
	ErrStatement error
}

func (m *MockedMongoDB) NotificationGetAll() ([]nmodel.Notification, error) {
	if m.ErrMap["NotificationGetAll"] {
		return nil, m.ErrStatement
	}

	if m.ErrMap["NotificationGetAllNotFound"] {
		return []nmodel.Notification{}, nil
	}

	result := []nmodel.Notification{}
	jsoned := nlib.Loadfixture("notification_all.json")
	err := json.Unmarshal(jsoned, &result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (m *MockedMongoDB) NotificationGetSpecific(id bson.ObjectId) (*nmodel.Notification, error) {
	if m.ErrMap["NotificationGetSpecific"] {
		return nil, m.ErrStatement
	}

	if m.ErrMap["NotificationGetSpecificNotFound"] {
		return &nmodel.Notification{}, nil
	}

	result := &nmodel.Notification{}
	jsoned := nlib.Loadfixture("notification_specific.json")
	err := json.Unmarshal(jsoned, result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (m *MockedMongoDB) NotificationInsert(notification *nmodel.Notification) (*nmodel.Notification, error) {
	if m.ErrMap["NotificationInsert"] {
		return nil, m.ErrStatement
	}

	notif := &nmodel.Notification{
		ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
		Name: "Group-Ops",
		Type: "telegram-bot",
		Config: map[string]interface{}{
			"bot_token": "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
			"recipients": []string{
				"-246367260",
			},
		},
	}
	return notif, nil
}

func (m *MockedMongoDB) NotificationUpdate(notification *nmodel.Notification) (*nmodel.Notification, error) {
	if m.ErrMap["NotificationUpdate"] {
		return nil, m.ErrStatement
	}

	notif := &nmodel.Notification{
		ID:   bson.ObjectIdHex("5977a69476371a0001a1d1ef"),
		Name: "Group-Ops",
		Type: "telegram-bot",
		Config: map[string]interface{}{
			"bot_token": "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11",
			"recipients": []string{
				"-246367260",
			},
		},
	}
	return notif, nil
}

func (m *MockedMongoDB) NotificationDelete(id bson.ObjectId) error {
	if m.ErrMap["NotificationDelete"] {
		return m.ErrStatement
	}

	if m.ErrMap["NotificationDeleteNotObjectId"] {
		return m.ErrStatement
	}

	return nil
}

func (m *MockedMongoDB) NotificationFieldsGetAll() ([]nmodel.NotificationField, error) {
	if m.ErrMap["NotificationFieldsGetAll"] {
		return nil, m.ErrStatement
	}

	if m.ErrMap["NotificationFieldsGetAllNotFound"] {
		return []nmodel.NotificationField{}, nil
	}

	result := []nmodel.NotificationField{}
	jsoned := nlib.Loadfixture("notification_fields_all.json")
	err := json.Unmarshal(jsoned, &result)

	if err != nil {
		return nil, err
	}

	return result, nil
}

type MockedNotifManager struct {
	ErrMap       map[string]bool
	ErrStatement error
}

func (nf *MockedNotifManager) Start() error {
	if nf.ErrMap["Start"] {
		return nf.ErrStatement
	}

	return nil
}

func (nf *MockedNotifManager) Restart() {
}

func TestGetAllNotification(t *testing.T) {
	mongodb := &MockedMongoDB{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}
	notifManager := &MockedNotifManager{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}

	// Create Handler Object
	handler := nhandler.NewHandler(mongodb, notifManager, "")

	// Setup HTTP Server
	r := mux.NewRouter()
	r.HandleFunc("/alert/notification", handler.GetAllNotification).Methods("GET")
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Hit API Endpoint
	targetPath := fmt.Sprintf("%v/%v", serverURL, "alert/notification")
	req, _ := http.NewRequest("GET", targetPath, nil)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to get worker status: %v", err)
	}
	defer resp.Body.Close()

	t.Run("GetAllNotification_OK", func(t *testing.T) {
		// Hit API Endpoint
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to get worker status: %v", err)
		}
		if resp.StatusCode != 200 {
			t.Fatalf("Response code %v", resp.StatusCode)
		}

		// Get the Body
		encodedBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("Unable to read response body: %v", err)
		}

		var notifs []nmodel.Notification
		err = extractContent(encodedBody, &notifs)

		// Check content
		contentLength := len(notifs)
		if contentLength != 3 {
			t.Fatalf("The content should contain 3 rows instead of %v", contentLength)
		}

		if notifs[0].Name != "Group-Ops" {
			t.Fatalf("First Notif Name should be Group-Ops but have: %v", notifs[0].Name)
		}

		resp.Body.Close()
	})

	// GetAllNotification Scenario, NOK
	t.Run("GetAllNotification_NOK", func(t *testing.T) {
		mongodb.ErrMap["NotificationGetAllNotFound"] = true
		// Hit API Endpoint
		resp, _ = http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		mongodb.ErrMap["NotificationGetAll"] = true
		// Hit API Endpoint
		resp, _ := http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})
}

func TestGetSpecificNotification(t *testing.T) {
	mongodb := &MockedMongoDB{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}
	notifManager := &MockedNotifManager{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}

	// Create Handler Object
	handler := nhandler.NewHandler(mongodb, notifManager, "")

	// Setup HTTP Server
	r := mux.NewRouter()
	r.HandleFunc("/alert/notification/{id}", handler.GetSpecificNotification).Methods("GET")
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Hit API Endpoint
	targetPath := fmt.Sprintf("%v/%v", serverURL, "alert/notification/59e6edcec79b098237220485")
	req, _ := http.NewRequest("GET", targetPath, nil)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to get worker status: %v", err)
	}
	defer resp.Body.Close()

	// GetSpesificNotification Scenario, OK
	t.Run("GetSpesificNotification_OK", func(t *testing.T) {
		// Hit API Endpoint
		resp, _ = http.DefaultClient.Do(req)
		if resp.StatusCode != 200 {
			t.Fatalf("Response code should not be 200.")
		}

		// Get the Body
		encodedBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("Unable to read response body: %v", err)
		}

		var notif nmodel.Notification
		err = extractContent(encodedBody, &notif)

		if notif.Name != "Group-Ops" {
			t.Fatalf("Notif Name should be Group-Ops but have: %v", notif.Name)
		}

		resp.Body.Close()
	})

	// GetSpesificNotification Scenario, NOK
	t.Run("GetSpesificNotification_NOK", func(t *testing.T) {
		mongodb.ErrMap["NotificationGetSpecificNotFound"] = true
		// Hit API Endpoint
		resp, _ = http.DefaultClient.Do(req)
		if resp.StatusCode != 200 {
			t.Fatalf("Response code should not be 200.")
		}

		mongodb.ErrMap["NotificationGetSpecific"] = true
		// Hit API Endpoint
		resp, _ := http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})

	// checkID Scenario, NOK
	t.Run("checkID_NOK", func(t *testing.T) {
		mongodb.ErrMap["NotObjectId"] = true
		// Hit API Endpoint
		stargetPath := fmt.Sprintf("%v/%v", serverURL, "alert/notification/isnotobjectid")
		req, _ := http.NewRequest("GET", stargetPath, nil)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to get worker status: %v", err)
		}
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})
}

func TestInsertNotification(t *testing.T) {
	mongodb := &MockedMongoDB{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}
	notifManager := &MockedNotifManager{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}

	// Create Handler Object
	handler := nhandler.NewHandler(mongodb, notifManager, "")

	// Setup Routing
	r := mux.NewRouter()
	r.HandleFunc("/alert/notification", handler.InsertNotification).Methods("POST")

	// Create httptest Server
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Hit API Endpoint
	targetPath := fmt.Sprintf("%v%v", serverURL, "/alert/notification")
	req, _ := http.NewRequest("POST", targetPath, nil)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to get worker status: %v", err)
	}
	defer resp.Body.Close()

	// Insert Scenario, OK
	t.Run("Get OK", func(t *testing.T) {
		// Hit API Endpoint
		var jsonRequest = []byte(`{
			"name":"Group-Ops",
			"type":"telegram-bot",
			"config":{
				"bot_token": "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11"
			},
			"recipients": [
				"-246367260"
			]
		}`)
		req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to get worker status: %v", err)
		}

		// Get the Body
		encodedBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("Unable to read response body: %v", err)
		}

		var notif nmodel.Notification
		err = extractContent(encodedBody, &notif)

		if notif.Name != "Group-Ops" {
			t.Fatalf("Notif Name should be Group-Ops but have: %v", notif.Name)
		}

		resp.Body.Close()
	})

	// Regex Error Scenario, NOK
	t.Run("Get Input Character NOK", func(t *testing.T) {
		// Hit API Endpoint
		var jsonRequest = []byte(`{"name":}`)
		req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
		resp, _ := http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})

	// Insert Scenario, NOK
	t.Run("Get Insert NOK", func(t *testing.T) {
		mongodb.ErrMap["NotificationInsert"] = true
		// Hit API Endpoint
		var jsonRequest = []byte(`{"name":"Group-Ops"}`)
		req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
		resp, _ := http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})
}

func TestUpdateNotification(t *testing.T) {
	mongodb := &MockedMongoDB{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}
	notifManager := &MockedNotifManager{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}

	// Create Handler Object
	handler := nhandler.NewHandler(mongodb, notifManager, "")

	// Setup Routing
	r := mux.NewRouter()
	r.HandleFunc("/alert/notification/{id}", handler.UpdateNotification).Methods("PUT")

	// Create httptest Server
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Hit API Endpoint
	targetPath := fmt.Sprintf("%v%v", serverURL, "/alert/notification/5977a69476371a0001a1d1ef")
	req, _ := http.NewRequest("PUT", targetPath, nil)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to get worker status: %v", err)
	}
	defer resp.Body.Close()

	// Insert Scenario, OK
	t.Run("Get OK", func(t *testing.T) {
		// Hit API Endpoint
		var jsonRequest = []byte(`{
			"name":"Group-Ops",
			"type":"telegram-bot",
			"config":{
				"bot_token": "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11"
			},
			"recipients": [
				"-246367260"
			]
		}`)
		req, _ := http.NewRequest("PUT", targetPath, bytes.NewBuffer(jsonRequest))
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to get worker status: %v", err)
		}

		// Get the Body
		encodedBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("Unable to read response body: %v", err)
		}

		var notif nmodel.Notification
		err = extractContent(encodedBody, &notif)

		if notif.Name != "Group-Ops" {
			t.Fatalf("Notif Name should be Group-Ops but have: %v", notif.Name)
		}

		resp.Body.Close()
	})

	// Regex Error Scenario, NOK
	t.Run("Get Input Character NOK", func(t *testing.T) {
		// Hit API Endpoint
		var jsonRequest = []byte(`{"name":}`)
		req, _ := http.NewRequest("POST", targetPath, bytes.NewBuffer(jsonRequest))
		resp, _ := http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})

	// Update Scenario, NOK
	t.Run("Get Update NOK", func(t *testing.T) {
		mongodb.ErrMap["NotificationUpdate"] = true
		// Hit API Endpoint
		var jsonRequest = []byte(`{"name":"Group-Ops"}`)
		req, _ := http.NewRequest("PUT", targetPath, bytes.NewBuffer(jsonRequest))
		resp, _ := http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})
}

func TestDeleteNotification(t *testing.T) {
	mongodb := &MockedMongoDB{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}
	notifManager := &MockedNotifManager{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}

	// Create Handler Object
	handler := nhandler.NewHandler(mongodb, notifManager, "")

	r := mux.NewRouter()
	r.HandleFunc("/alert/notification/{id}", handler.DeleteNotification).Methods("DELETE")
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Hit API Endpoint
	targetPath := fmt.Sprintf("%v/%v", serverURL, "alert/notification/5977a69476371a0001a1d1ef")
	req, _ := http.NewRequest("DELETE", targetPath, nil)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to get worker status: %v", err)
	}
	defer resp.Body.Close()

	// Delete Scenario, OK
	t.Run("Get OK", func(t *testing.T) {
		// Hit API Endpoint
		req, _ := http.NewRequest("DELETE", targetPath, nil)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to get worker status: %v", err)
		}
		if resp.StatusCode != 200 {
			t.Fatalf("Response code %v", resp.StatusCode)
		}

		resp.Body.Close()
	})

	// Delete Scenario, NOK
	t.Run("Get NOK", func(t *testing.T) {
		mongodb.ErrMap["NotificationDelete"] = true
		// Hit API Endpoint
		req, _ := http.NewRequest("DELETE", targetPath, nil)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to get worker status: %v", err)
		}
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})

	// checkID Scenario, NOK
	t.Run("Get NOK", func(t *testing.T) {
		mongodb.ErrMap["NotificationDelete"] = false
		mongodb.ErrMap["NotificationDeleteNotObjectId"] = true
		// Hit API Endpoint
		stargetPath := fmt.Sprintf("%v/%v", serverURL, "alert/notification/isnotobjectid")
		req, _ := http.NewRequest("DELETE", stargetPath, nil)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to get worker status: %v", err)
		}
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})
}

func TestGetAllNotificationFields(t *testing.T) {
	mongodb := &MockedMongoDB{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}
	notifManager := &MockedNotifManager{
		ErrStatement: fmt.Errorf(ntesting.IntentionallyError),
		ErrMap:       map[string]bool{},
	}

	// Create Handler Object
	handler := nhandler.NewHandler(mongodb, notifManager, "")

	// Setup HTTP Server
	r := mux.NewRouter()
	r.HandleFunc("/alert/notification/fields", handler.GetAllNotificationFields).Methods("GET")
	httpServer := ntesting.Setup(r)
	defer httpServer.Close()
	serverURL, _ := url.Parse(httpServer.URL)

	// Hit API Endpoint
	targetPath := fmt.Sprintf("%v/%v", serverURL, "alert/notification/fields")
	req, _ := http.NewRequest("GET", targetPath, nil)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("Unable to get worker status: %v", err)
	}
	defer resp.Body.Close()

	t.Run("GetAllNotificationFields_OK", func(t *testing.T) {
		// Hit API Endpoint
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatalf("Unable to get worker status: %v", err)
		}
		if resp.StatusCode != 200 {
			t.Fatalf("Response code %v", resp.StatusCode)
		}

		// Get the Body
		encodedBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("Unable to read response body: %v", err)
		}

		var notifs []nmodel.NotificationField
		err = extractContent(encodedBody, &notifs)

		// Check content
		contentLength := len(notifs)
		if contentLength != 2 {
			t.Fatalf("The content should contain 2 rows instead of %v", contentLength)
		}

		if notifs[0].Type != "telegram-bot" {
			t.Fatalf("First Notif Name should be telegram-bot but have: %v", notifs[0].Type)
		}

		resp.Body.Close()
	})

	// GetAllNotificationFields Scenario, NOK
	t.Run("GetAllNotificationFields_NOK", func(t *testing.T) {
		mongodb.ErrMap["NotificationFieldsGetAllNotFound"] = true
		// Hit API Endpoint
		resp, _ = http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		mongodb.ErrMap["NotificationFieldsGetAll"] = true
		// Hit API Endpoint
		resp, _ := http.DefaultClient.Do(req)
		if resp.StatusCode == 200 {
			t.Fatalf("Response code should not be 200.")
		}

		resp.Body.Close()
	})
}

func extractContent(encodedBody []byte, content interface{}) error {
	// Unmarshal JSON Resp
	var body nrespwriter.ResponseFormat
	err := json.Unmarshal(encodedBody, &body)
	if err != nil {
		return fmt.Errorf("Unable to unmarshal json response: %v", err)
	}

	encodedContent, _ := json.Marshal(body.Data)
	err = json.Unmarshal(encodedContent, &content)
	if err != nil {
		return fmt.Errorf("Unable to unmarshal content: %v", err)
	}
	return nil
}
