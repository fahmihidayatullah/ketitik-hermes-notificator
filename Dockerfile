FROM alpine
MAINTAINER Developer Ketitik <developer@ketitik.com>

# Install Dep
# CA
RUN apk add --no-cache ca-certificates 

# Working Directory
WORKDIR /home/hermes-notificator

# Copy in the source
COPY _bin/hermes-notificator-busybox ./hermes-notificator-busybox

# Make shell scripts executable
RUN chmod +x ./hermes-notificator-busybox

# Running
CMD ["./hermes-notificator-busybox"]