# Notification Service
This service acts as broker to the third party notificator such as Telegram Bot, Mailduck, SMTP, SMS Provider, etc.
It provides webhook or HTTP POST listener endpoint that is used by services which need to send notificaiton. 

## Status
Under Development

## How to Start Development
```bash
$ cd $GOPATH/src/
$ mkdir -pv ketitik/
$ cd ketitik/
$ git clone git@bitbucket.org:ketitik/hermes-notificator.git notificator
```

## To Build and Run (For Development)
Modify the _bin/notification.yaml
```bash
# Notificator Configuration
service_data:
  notificator_address: localhost:8080
  notificator_mgmt_address: localhost:8081
  
source_data:
  mongodb_server: localhost:27017
  mongodb_name: db
  mongodb_username: user
  mongodb_password: pass
  mongodb_timeout: 5
```

Then, execute
```bash
(../notificator/) $ ./_script/build.sh
(../notificator/) $ ./_script/run.sh
```

## License
Ketitik's Proprietary.
