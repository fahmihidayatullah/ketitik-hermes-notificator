package main

import (
	"net/http"

	nconfig "ketitik/shared-service/hermes-notificator/config"
	nmongo "ketitik/shared-service/hermes-notificator/db/mongo"
	nhandler "ketitik/shared-service/hermes-notificator/handler"
	nnotif "ketitik/shared-service/hermes-notificator/notificator"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

const (
	// VERSION of this service.
	VERSION = "1.0.1"
	// DEVELOPER of this service.
	DEVELOPER = "Amoeba Ketitik"
	// ConfigFileLocation is the file configuration of ths service.
	ConfigFileLocation = "notification.yaml"
	ketitikASCIIImage  = `
	888  888 88888888 88888888 88888888 88888888 88888888 888  888
	888  888 88          88       88       88       88    888  888
	888 888  88          88       88       88       88    888 888
	88888    88888888    88       88       88       88    88888
	888 888  88          88       88       88       88    888 888
	888  888 88          88       88       88       88    888  888
	888  888 88888888    88    88888888    88    88888888 888  888
	`
)

/*
- Read configuration file
- Connect to mongodb
- Create notificator API handler
- Inject notificator struct
- Start notificator concurrently
- Start notificator API handler
*/

// Handler hold the function handler for API's endpoint.
type Handler interface {
	GetAllNotification(w http.ResponseWriter, r *http.Request)
	GetSpecificNotification(w http.ResponseWriter, r *http.Request)
	InsertNotification(w http.ResponseWriter, r *http.Request)
	UpdateNotification(w http.ResponseWriter, r *http.Request)
	DeleteNotification(w http.ResponseWriter, r *http.Request)
	GetAllNotificationFields(w http.ResponseWriter, r *http.Request)
}

// NewRouter returns router.
func NewRouter(handler Handler) *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/alert/notification", handler.GetAllNotification).Methods("GET")
	r.HandleFunc("/alert/notification/fields", handler.GetAllNotificationFields).Methods("GET")
	r.HandleFunc("/alert/notification/{id:[a-zA-Z0-9\\.\\-_]+}", handler.GetSpecificNotification).Methods("GET")
	r.HandleFunc("/alert/notification", handler.InsertNotification).Methods("POST")
	r.HandleFunc("/alert/notification/{id:[a-zA-Z0-9\\.\\-_]+}", handler.UpdateNotification).Methods("PUT")
	r.HandleFunc("/alert/notification/{id:[a-zA-Z0-9\\.\\-_]+}", handler.DeleteNotification).Methods("DELETE")

	return r
}

func main() {
	// Pre-printed text at startup.
	logrus.Printf("Notificator Service v%v", VERSION)
	logrus.Printf("Developed by %v.\n%v", DEVELOPER, ketitikASCIIImage)
	logrus.Printf("Start service...")

	// Get Config
	configLoader := nconfig.NewYAMLConfigLoader(ConfigFileLocation)
	config, err := configLoader.GetConfig()
	if err != nil {
		logrus.Fatalf("Unable to load configuration: %v", err)
	}

	// mongodb for connect to mongo database
	mongodb, err := nmongo.NewMongoDB(config.SourceData.MongoDBServer, config.SourceData.MongoDBName,
		config.SourceData.MongoDBUsername, config.SourceData.MongoDBPassword, config.SourceData.MongoDBTimeout)
	if err != nil {
		logrus.Fatalf("Unable to create MongoDB instance: %v", err)
	}

	logrus.Printf("MongoDB Connection Test: PASS | Mongo Version: %v.", mongodb.GetVersion())

	notifManager := nnotif.NewNotifManager(mongodb, ConfigFileLocation)
	go notifManager.Start()

	handler := nhandler.NewHandler(mongodb, notifManager, ConfigFileLocation)
	r := NewRouter(handler)

	// Run Web Server
	logrus.Printf("Starting notificator management http server at %v", config.ServiceData.NotificatorMgmtAddress)
	err = http.ListenAndServe(config.ServiceData.NotificatorMgmtAddress, r)
	if err != nil {
		logrus.Fatalf("Unable to run notificator management  http server: %v", err)
	}
	logrus.Printf("Stopping notificator management service...")
}
